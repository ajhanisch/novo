from .network import Network

class Environment:
    def __init__(self, id):
        self.id = id

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        if len(str(id)) != 4: raise Exception("id must be 4 digit number")
        if int(id) % 2 != 0: raise Exception("id must be even number")
        if int(id) > 4092: raise Exception("id must be smaller than 4092")
        self._id = id
    
class Opennebula(Environment):
    def __init__(self, id):
        super().__init__(id)