class Network:
    def __init__(self, id, network_octet_first, network_octet_fourth, dns, subnet_mask, gateway):
        self.id = str(id)
        self.network_octet_first = network_octet_first
        self.network_octet_second = False
        self.network_octet_third = False
        self.network_octet_fourth = network_octet_fourth
        self.wan = False
        self.lan = False
        self.network_gateway = gateway


        self.network_octet_first = str(network_octet_first)
        self.set_network_octet_second()
        self.set_network_octet_third()
        self.network_octet_fourth = str(network_octet_fourth)
        self.set_address()
        self.network_dns = str(network_dns)
        self.network_subnet_mask = str(network_subnet_mask)
        self.network_gateway = str(network_gateway)
        self.set_network_gateway()

    # def __repr__(self):
    #     return (
    #     'Network('
    #             'id={0.id},'
    #             'network_octet_first={0.network_octet_first},'
    #             'network_octet_second={0.network_octet_second},'
    #             'network_octet_third={0.network_octet_third},'
    #             'network_octet_fourth={0.network_octet_fourth},'
    #             'address={0.address},'
    #             'network_dns={0.network_dns},'
    #             'network_subnet_mask={0.network_subnet_mask},'
    #             'network_gateway={0.network_gateway},'
    #             'wan={0.wan},'
    #             'lan={0.lan}'
    #         ')'
    #         .format(self)
    #     )

    def set_network_octet_second(self):
        self.network_octet_second = str(int(self.id[0:2]))

    def set_network_octet_third(self):
        if self.wan:
            self.network_octet_third = str(int(self.id[2:4]))
        
        if self.lan:
            self.network_octet_third = str(int(self.id[2:4]) + 1)

    def set_address(self):
        address = [
            self.network_octet_first,
            self.network_octet_second,
            self.network_octet_third,
            self.network_octet_fourth
        ]

        self.address = '.'.join(address)

    def set_network_gateway(self):
        network_gateway = [
            str(self.network_octet_first),
            str(self.network_octet_second),
            str(self.network_octet_third),
            str(self.network_gateway)
        ]

        self.network_gateway = '.'.join(network_gateway)