import logging
from novo import api
from novo import host
import logging.config
from novo import network
from config import Config
from novo import environment

def main():
    """ Parse arguments provided """
    args = Config.args

    """ Set logging configuration from config.py """
    logging.config.dictConfig(Config.LOGGING_CONFIG)

    """ Gather __main__ module logger configuration from config.py """
    log = logging.getLogger(__name__)
    
    """ Process command line """
    for id in args.id:
        env = environment.Opennebula(id=id)
        print(env.id)

    """ Exit successfully """
    exit(0)

if __name__ == "__main__":
    main()